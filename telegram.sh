#!/bin/sh
data="$@"

# Telegram Bot
apiToken=""
userChatId=""

sendMessage() {
  text="$(echo "${@}" | sed 's:\\n:\n:g')"
  echo "Sending messgaae: $text"

  curl -s \
    -X POST \
    https://api.telegram.org/bot$apiToken/sendMessage \
	--data-urlencode "text=$text" \
    -d "chat_id=$userChatId"
}

sendFile() {
  echo "Sending file: $1"
  curl -s \
    -X POST \
    https://api.telegram.org/bot$apiToken/sendDocument \
    -F chat_id="$userChatId" \
	-F document=@"$1"
}

sendPhoto() {
  echo "Sending Photo: $1"
  caption="$(hostname)/$(date)"
  curl -s \
    -X POST \
    https://api.telegram.org/bot$apiToken/sendPhoto \
    -F chat_id="$userChatId" \
    -F photo="@${1}" \
	-F caption="${caption}" 
}

sendMessage $data
