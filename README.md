# chrome alter watcher

See `go run . -h`.

Example:

	go run main.go \
		-url="https://twitter.com/vocdoni/status/1240635806626299915" \
		-xpath "//*[text()[contains(., 'scalable voting system')]]" \
		-alertscript=./telegram.sh
