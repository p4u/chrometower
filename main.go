package main

// docker pull chromedp/headless-shell:latest
// docker run -d -p 9222:9222 --rm --name headless-shell chromedp/headless-shell

import (
	"context"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os/exec"
	"strings"
	"sync/atomic"
	"time"

	"github.com/chromedp/chromedp"
)

//const selector = "#page-body > div > div.center-content > p:nth-child(2) > span"
//const sucessText = "El teu vot ha estat registrat"

var goodResults = int32(0)
var badResults = int32(0)
var tries = int32(0)
var maxtries = 0
var attempts = int32(0)
var alertscript = ""
var working = int32(0)
var text = ""
var debug = false

func main() {
	xpath := flag.String("xpath", "", "the xpath query that must match")
	url := flag.String("url", "", "browser URL to check")
	flag.StringVar(&text, "text", "", "set text that xpath selector must conaint")
	remote := flag.String("remote", "", "set remote chromedp instance URL to connect with")
	timeout := flag.Duration("timeout", 10*time.Second, "duration to wait before considering the page failed to load or the selector did not match")
	period := flag.Duration("period", 30*time.Second, "duration between checks")
	flag.StringVar(&alertscript, "alertscript", "", "script to execute when an alert should be sent")
	flag.IntVar(&maxtries, "tries", 2, "number of atempts before sending an alert")
	flag.BoolVar(&debug, "debug", false, "do not use headless chrome")
	flag.Parse()

	if len(*url) == 0 {
		log.Fatal("parameter -url must be defined")
	}

	log.Printf("monitorizing URL: %s", *url)
	log.Printf("xpath: %q", *xpath)
	if alertscript != "" {
		log.Printf("alert script: %s", alertscript)
	}
	ctx := context.Background()
	ctx = browser(ctx, *remote)

	for {
		ctx, cancel := context.WithTimeout(ctx, *timeout)
		open(ctx, *url, *xpath)
		cancel()

		good := atomic.LoadInt32(&goodResults)
		bad := atomic.LoadInt32(&badResults)
		if good+bad > 0 {
			log.Printf("Good:%d Bad:%d | Success ratio: %d%% ", good, bad, good*100/(good+bad))
		}

		time.Sleep(*period)
	}
}

func browser(ctx context.Context, remote string) context.Context {
	// Don't run any cancel funcs, as those kill the browser.
	if remote != "" {
		log.Printf("connecting to remote chromedp %s", remote)
		ctx, _ = chromedp.NewRemoteAllocator(ctx, remote)
	} else {
		log.Println("creating new browser")
		if debug {
			ctx, _ = chromedp.NewExecAllocator(ctx)
		}
	}
	ctx, _ = chromedp.NewContext(ctx)

	// Start (or connect to) the browser.
	if err := chromedp.Run(ctx); err != nil {
		log.Fatalf("cannot open browser: %s", err)
	}
	return ctx
}

func open(ctx context.Context, url, xpath string) {
	if atomic.LoadInt32(&working) > 0 {
		log.Printf("browser is busy, skipping test")
		return
	}
	atomic.AddInt32(&working, 1)
	defer atomic.AddInt32(&working, -1)

	// create a new tab
	ctx, cancel := chromedp.NewContext(ctx)
	defer cancel()

	var buf []byte
	var stext string
	err := chromedp.Run(ctx,
		chromedp.Navigate(url),
		// Use WaitReady instead of WaitVisible, since many text queries
		// will also match non-visible nodes like the page title.
		chromedp.WaitReady(xpath),
		chromedp.OuterHTML(xpath, &stext),
		chromedp.CaptureScreenshot(&buf),
	)
	if err == nil {
		if strings.Contains(stext, text) {
			atomic.AddInt32(&goodResults, 1)
		} else {
			atomic.AddInt32(&badResults, 1)
			go sendAlert(fmt.Sprintf("text does not match: %s", stext))
			go saveFile("screenshot.png", &buf)
			log.Printf("text does not match: %s", stext)
		}
	} else {
		atomic.AddInt32(&badResults, 1)
		go sendAlert(fmt.Sprintf("error detected: %s", err))
		go saveFile("screenshot.png", &buf)
		log.Printf("error: %s", err)
	}
}

func logs(s string, i ...interface{}) {
	log.Printf("Console: %s", i)
}

func saveFile(filename string, buf *[]byte) {
	if len(*buf) == 0 {
		return
	}
	if err := ioutil.WriteFile(filename, *buf, 0644); err != nil {
		log.Printf("warning: cannot save screenshot\n")
	} else {
		log.Printf("taking screenshot, saving on %s", filename)
	}
}

func sendAlert(msg string) {
	if len(alertscript) == 0 {
		return
	}
	a := atomic.LoadInt32(&attempts)
	if a >= int32(maxtries) {
		atomic.StoreInt32(&attempts, 0)
		log.Println("executing alert script")
		if err := exec.Command(alertscript, msg).Start(); err != nil {
			log.Printf("cannot execute script alert: %s", err)
		}
	} else {
		atomic.AddInt32(&attempts, 1)
	}
}
